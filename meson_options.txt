option('android',
        type : 'boolean',
        value : false,
        description : 'Compile libcamera with Android Camera3 HAL interface')

option('documentation',
        type : 'boolean',
        description : 'Generate the project documentation')

option('gstreamer',
        type : 'feature',
        value : 'auto',
        description : 'Compile libcamera GStreamer plugin')

option('test',
        type : 'boolean',
        description: 'Compile and include the tests')

option('v4l2',
        type : 'boolean',
        value : false,
        description : 'Compile the V4L2 compatibility layer')
